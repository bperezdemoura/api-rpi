import os
import datetime
import RPi.GPIO as GPIO
from flask import Flask, request, jsonify, Response
from flask_cors import CORS

GPIO.setmode(GPIO.BCM) #GPIO.BCM significa que eu seleciono o GPIO, nao o pino. se fosse GPIO.BOARD seria o contrario

GPIO.setup(23, GPIO.OUT) #pino 16
GPIO.setup(24, GPIO.OUT) #pino 18


app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
  return "hello world!"

@app.route('/temperatura', methods=['GET'])
def temperatura():
  now = datetime.datetime.now()
  temperatura = str(os.popen("vcgencmd measure_temp").read())
  return jsonify(temperatura = float(temperatura[5:-3]), timestamp = now )

@app.route('/lampada/cozinha', methods=['POST'])
def lampadaCozinha():
  print(request.json)
  acao = request.json['acao']
  if(acao == "ligar"):
    GPIO.output(23, 1)
    return "ok"
  elif(acao == "desligar"):
    GPIO.output(23, 0)
    return "ok"
  else:
    return("acao nao reconhecida", 400)

@app.route('/lampada/sala', methods=['POST'])
def lampadaSala():
  print(request.json)
  acao = request.json['acao']
  if(acao == "ligar"):
    GPIO.output(24, 1)
    return "ok"
  elif(acao == "desligar"):
    GPIO.output(24, 0)
    return "ok"
  else:
    return("acao nao reconhecida", 400)


if __name__=="__main__":
    app.run(host= '0.0.0.0', port = 5000, debug=True, threaded=True)

