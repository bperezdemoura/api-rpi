Passos para instalar as dependencias:

* sudo apt-get install python3-pip
* pip3 install flask
* pip3 install -U flask-cors
* pip3 install RPi.GPIO

a API fica disponivel no IP da raspberry, porta 5000.
Ela possui 3 endpoints:

* /temperatura #GET
* /lampada/cozinha #POST
* /lampada/sala #POST

##/temperatura #GET
  tem como response um JSON:
```json
{
  "temperatura": float,
  "timestamp": datetime
}
```

##/lampada/cozinha #POST
###\#POST
  tem como request um JSON:
```json
{
  "acao": string
}
```
  tem como response uma confirmacao: "ok"

##/lampada/sala #POST
  tem como request um JSON:
```json
{
  "acao": string
}
```
  tem como response uma confirmacao: "ok"
  
  se receber algo diferente de "ligar" ou "desligar" retorna uma mensagem de erro e codigo 400 bad request



